package com.example.gpro.RendementEmploye;


import com.example.gpro.ProductionSection.ProductionSection;
import com.example.gpro.chaine.Chaine;
import com.example.gpro.section.Section;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class SearchRendementRepository {
    EntityManager em;

    public Boolean estNonVide(Object value){
        if(value.equals("") || value==null  || value.equals("undefined")|| value.equals("null")){
            return false;
        }else{
            return true;
        }
    }

    public Boolean estNonVide(Date value){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dat = dateFormat.format(value);
        if(dat.toString().equals("") || dat==null    || dat.equals("null")|| dat.equals("21-07-1983")){
            return false;
        }else{
            return true;
        }
    }


    public SearchRendementRepository(EntityManager em) {
        this.em = em;
    }

    List<RendementEmploye> findRendementByObjectSearchRendement(ObjectSearchRendement objectSearchRendement){
        CriteriaBuilder cb=em.getCriteriaBuilder();
        CriteriaQuery<RendementEmploye> cq=cb.createQuery(RendementEmploye.class);

        Root<RendementEmploye> rendementRoot=cq.from(RendementEmploye.class);

        Join<RendementEmploye,ProductionSection> join1=rendementRoot.join("productionSection", JoinType.INNER);
        Join<ProductionSection,Section> join2=join1.join("section",JoinType.INNER);
        Join<Section,Chaine> join3=join2.join("chaine",JoinType.INNER);

        Predicate matriculePredicate=cb.equal(rendementRoot.get("matricule"),objectSearchRendement.getMatricule());
        Predicate  stratPredicate=cb.greaterThanOrEqualTo(join1.get("dateObs"),objectSearchRendement.getStart());
        Predicate  stopPredicate=cb.lessThanOrEqualTo(join1.get("dateObs"),objectSearchRendement.getStop());
        Predicate  nomsectionPredicate=cb.equal(join2.get("nom"),objectSearchRendement.getNomSection());
        Predicate nomchainePredicate=cb.equal(join3.get("nom"),objectSearchRendement.getNomChaine());

        List<Predicate> whereClause = new ArrayList<>();

        if(estNonVide(objectSearchRendement.getMatricule())){
            whereClause.add(matriculePredicate);
        }
        if (estNonVide(objectSearchRendement.getNomChaine())){
            whereClause.add(nomchainePredicate);
        }
        if (estNonVide(objectSearchRendement.getNomSection())){
            whereClause.add(nomsectionPredicate);
        }
        if (estNonVide(objectSearchRendement.getStart())){
            whereClause.add(stratPredicate);
        }
        if (estNonVide(objectSearchRendement.getStop())){
            whereClause.add(stopPredicate);
        }

        cq.where(whereClause.toArray(new Predicate[]{}));

        TypedQuery<RendementEmploye> query=em.createQuery(cq);
        return query.getResultList();




    }











    }
