package com.example.gpro.RendementEmploye;

import com.example.gpro.ProductionSection.ProductionSection;
import com.example.gpro.chaine.Chaine;
import com.example.gpro.section.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;
@CrossOrigin(origins = "*", allowedHeaders = "*")

@RestController
public class RendementEmployeController {

    @Autowired
    private RendemEntemployeService rendemEntemployeService;
    @RequestMapping("RendementEmployebetween2DateforEachDay/{matricule}/{start}/{stop}")
    public List<Object[]> getRendementEmployebetween2DateforEachDay(@PathVariable String matricule,  @PathVariable java.sql.Date start, @PathVariable java.sql.Date stop){
        return rendemEntemployeService.getRendementEmployebetween2DateforEachDay(start,stop,matricule);
    }
   /* @RequestMapping("RendementEmployebetween2DateforEachWeek/{matricule}/{start}/{stop}")
    public List<Object[]> getRendementEmployebetween2DateforEachWeek(@PathVariable String matricule,  @PathVariable java.sql.Date start, @PathVariable java.sql.Date stop){
        return rendemEntemployeService.getRendementEmployebetween2DateforEachWeek(start,stop,matricule);
    }*/
    @RequestMapping("RendementEmployebetween2DateforEachMonth/{matricule}/{start}/{stop}")
    public List<Object[]> getRendementEmployebetween2DateforEachMonth(@PathVariable String matricule,  @PathVariable java.sql.Date start, @PathVariable java.sql.Date stop){
        return rendemEntemployeService.getRendementEmployebetween2DateforEachMonth(start,stop,matricule);
    }
    @RequestMapping("rendementemployePositif/{chaineId}")
    public List<RendementEmploye> getRendementPositif(@PathVariable Long chaineId){
        return rendemEntemployeService.getRendementPositif(chaineId);
    }
    @RequestMapping("rendementemployeNegatif/{chaineId}")
    public List<RendementEmploye> getRendementNegatif(@PathVariable Long chaineId){
        return rendemEntemployeService.getRendementNegatif(chaineId);
    }
    @RequestMapping("/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionSectionId}/rendementemploye")
    public List<RendementEmploye> getAllRendementEmploye(@PathVariable Long chaineId, @PathVariable Long sectionId, @PathVariable Long productionSectionId){
        return rendemEntemployeService.getAllRendementEmployes(chaineId,sectionId,productionSectionId);
    }
    @RequestMapping("/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/rendementemploye/{id}")
    public Optional<RendementEmploye> getRendementEmploye(@PathVariable Long chaineId, @PathVariable Long sectionId, @PathVariable Long productionsectionId, @PathVariable Long id){
        return rendemEntemployeService.getRendementEmploye(chaineId,sectionId,productionsectionId,id);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/rendementemploye", method = RequestMethod.POST)
    public void addRendementEmploye(@RequestBody RendementEmploye rendementEmploye, @PathVariable Long chaineId, @PathVariable Long sectionId, @PathVariable Long productionsectionId){
        rendementEmploye.setProductionSection(new ProductionSection(productionsectionId,new Date(),new Section(sectionId,"",false,new Chaine(chaineId,"",null),null),null,null,null));
        rendemEntemployeService.addRendementEmploye(rendementEmploye);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/rendementemploye/{id}", method = RequestMethod.PUT)
    public void updateRendementEmploye(@RequestBody RendementEmploye rendementEmploye, @PathVariable Long chaineId, @PathVariable Long sectionId,@PathVariable Long productionsectionId ,@PathVariable Long id){
        rendementEmploye.setProductionSection(new ProductionSection(productionsectionId,new Date(),new Section(sectionId,"",false,new Chaine(chaineId,"",null),null),null,null,null));
        rendemEntemployeService.updateRendementEmploye(id,rendementEmploye);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/rendementemploye/{id}", method = RequestMethod.DELETE)
    public void deleteRendementEmploye(@PathVariable Long id){
        rendemEntemployeService.deleteRendementEmploye(id);
    }


    @RequestMapping(value = "resultRendement",method = RequestMethod.POST)
    public List<RendementEmploye> searchRendement(@RequestBody ObjectSearchRendement objectSearch){

        return rendemEntemployeService.searchRendement(objectSearch);
    }

}

