package com.example.gpro.RendementEmploye;

import com.example.gpro.ProductionSection.ProductionSection;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
public class RendementEmploye {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",updatable = false,nullable = false)
    Long id;
    @NotNull
    @Valid
    String matricule;
    @NotNull
    @Valid
    String nom;
    @NotNull
    @Valid
    String prenom;
    @NotNull
    @Valid
    Float rendement;
    @NotNull
    @Valid
    String status;

    @ManyToOne
    private ProductionSection productionSection;



    public RendementEmploye(Long id,String matricule, String nom, String prenom, Float rendement, String status, ProductionSection productionSection) {
        this.id=id;

        this.matricule = matricule;
        this.nom = nom;
        this.prenom = prenom;
        this.rendement = rendement;
        this.status = status;
        this.productionSection = productionSection;
    }

    public RendementEmploye() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Float getRendement() {
        return rendement;
    }

    public void setRendement(Float rendement) {
        this.rendement = rendement;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProductionSection getProductionSection() {
        return productionSection;
    }

    public void setProductionSection(ProductionSection productionSection) {
        this.productionSection = productionSection;
    }
}
