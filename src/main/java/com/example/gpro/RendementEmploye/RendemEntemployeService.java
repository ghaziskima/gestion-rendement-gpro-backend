package com.example.gpro.RendementEmploye;

import com.example.gpro.ProductionSection.ProductionSection;
import com.example.gpro.qualité.ObjectSearchQualite;
import com.example.gpro.qualité.Qualite;
import com.example.gpro.section.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RendemEntemployeService {


    @Autowired
    private RendemEntemployeRepository rendemEntemployeRepository;
    @Autowired
    private SearchRendementRepository searchRendementRepository;

    public List<RendementEmploye> getAllRendementEmployes(Long chaineId, Long sectionId, Long productionSectionId) {
        List<RendementEmploye> rendementEmployes=new ArrayList<>();
        rendemEntemployeRepository.findByProductionSectionId(productionSectionId).forEach(rendementEmployes::add);
        return rendementEmployes;
    }

    public Optional<RendementEmploye> getRendementEmploye(Long chaineId, Long sectionId, Long productionsectionId, Long id) {
        return rendemEntemployeRepository.findById(id);
    }

    public void addRendementEmploye(RendementEmploye rendementEmployes) {
        rendemEntemployeRepository.save(rendementEmployes);
    }

    public void updateRendementEmploye(Long id, RendementEmploye rendementEmployes) {
        rendemEntemployeRepository.save(rendementEmployes);
    }

    public void deleteRendementEmploye(Long id) {
        rendemEntemployeRepository.deleteById(id);
    }

    public List<RendementEmploye> getRendementPositif(Long chaineId) {
        List<RendementEmploye> rendementEmployes=new ArrayList<>();
        rendemEntemployeRepository.findByStatusLikeAndProductionSectionDateObsAndProductionSectionSectionChaineIdAndProductionSectionSectionReelTrue("%"+"Positif"+"%", new Date()/*java.sql.Date.valueOf("2020-07-24")*/,chaineId).forEach(rendementEmployes::add);
        return rendementEmployes;

    }
    public List<RendementEmploye> getRendementNegatif(Long chaineId) {
        List<RendementEmploye> rendementEmployes=new ArrayList<>();
        rendemEntemployeRepository.findByStatusLikeAndProductionSectionDateObsAndProductionSectionSectionChaineIdAndProductionSectionSectionReelTrue("%"+"Négatif"+"%", new Date()/*java.sql.Date.valueOf("2020-07-24")*/,chaineId).forEach(rendementEmployes::add);
       // rendemEntemployeRepository.findByProductionSectionSectionChaineId(chaineId).forEach(rendementEmployes::add);
        return rendementEmployes;

    }

    public List<Object[]> getRendementEmployebetween2DateforEachDay(java.sql.Date start, java.sql.Date stop, String matricule) {
        List<Object[]> rendementEmpl=new ArrayList<>();
        rendemEntemployeRepository.getRendementEmplbetween2DateForEachDay(start,stop,matricule).forEach(rendementEmpl::add);
        return rendementEmpl;
    }
    public List<Object[]> getRendementEmployebetween2DateforEachMonth(java.sql.Date start, java.sql.Date stop, String matricule) {
        List<Object[]> rendementEmpl=new ArrayList<>();
        rendemEntemployeRepository.getRendementEmplbetween2DateForEachMonth(start,stop,matricule).forEach(rendementEmpl::add);
        return rendementEmpl;
    }

    public List<RendementEmploye> searchRendement(ObjectSearchRendement objectSearchRendement) {
        List<RendementEmploye> resulRendements=new ArrayList<>();

        searchRendementRepository.findRendementByObjectSearchRendement(objectSearchRendement).forEach(resulRendements::add);
        return resulRendements;

    }

  /*  public List<Object[]> getRendementEmployebetween2DateforEachWeek(java.sql.Date start, java.sql.Date stop, String matricule) {

        List<Object[]> rendementEmpl=new ArrayList<>();
        List<Object[]> rendementEmplForEachWeek=new ArrayList<>();


        rendemEntemployeRepository.getRendementEmplbetween2DateForEachDay(start,stop,matricule).forEach(rendementEmpl::add);
/*
        int compteur=0;
        int nbrWeek=1;
        double rendement=0.0;
        for(Object[] rendementemplObject:rendementEmpl){
            rendement=rendement+rendementemplObject
        }

        for(int i=0;i<rendementEmpl.size();i++){
            rendement=rendement+rendementEmpl.get(i);

            if(compteur==7){
                compteur=0;
                nbrWeek++;
            }else{
                compteur++;
            }
        }
        return rendementEmplForEachWeek;
    }*/
}