package com.example.gpro.RendementEmploye;

import com.example.gpro.ProductionSection.ProductionSection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.Date;
import java.util.List;

@Repository
public interface RendemEntemployeRepository extends CrudRepository<RendementEmploye,Long> {
    public List<RendementEmploye> findByProductionSectionId(Long productionSectionId);
 //   public List<RendementEmploye> findByProductionSectionDateObs();
     @Query("select r from RendementEmploye r  where r.status like :x ")
     public List<RendementEmploye> findByStatus(@Param("x") String status);

    public List<RendementEmploye> findByStatusLikeAndProductionSectionDateObsAndProductionSectionSectionChaineIdAndProductionSectionSectionReelTrue( String status,Date date,Long ChaineId);

    //public List<RendementEmploye> findByProductionSectionSectionChaineId(Long ChaineId);
    @Query(value = "select  ps.date_Obs as dateObs,re.rendement as rendement,re.nom as nomemp,re.prenom as prenomemp ,ch.nom as nomch,s.nom as nomsect from Rendement_Employe re " +
            "inner join Production_Section ps " +
            "on ps.id = re.production_section_id " +
            "inner join Section s " +
            "on s.id = ps.section_id " +
            "inner join Chaine ch " +
            "on ch.id = s.chaine_id " +
            "WHERE re.matricule = :matricule  "+
            "AND :stop >= ps.date_Obs AND ps.date_Obs >= :start  "+
            "group by ch.nom, ps.date_Obs,re.rendement,re.nom,re.prenom ,s.nom order by ps.date_Obs", nativeQuery = true)
    public List<Object[]> getRendementEmplbetween2DateForEachDay(@Param("start")java.sql.Date start,@Param("stop") java.sql.Date stop,@Param("matricule") String matricule);

    @Query(value = "select  extract(year from  ps.date_Obs ) as yearDateObs,extract(month from  ps.date_Obs )as monthDateObs,sum(re.rendement)/count(re.rendement) as rendement,re.nom as nomemp,re.prenom as prenomemp ,ch.nom as nomch,s.nom as nomsect from Rendement_Employe re " +
            "inner join Production_Section ps " +
            "on ps.id = re.production_section_id " +
            "inner join Section s " +
            "on s.id = ps.section_id " +
            "inner join Chaine ch " +
            "on ch.id = s.chaine_id " +
            "WHERE re.matricule = :matricule  "+
            "AND :stop >= ps.date_Obs AND ps.date_Obs >= :start  "+
            "group by ch.nom,extract(year from  ps.date_Obs ),extract(month from  ps.date_Obs ),re.nom,re.prenom ,s.nom order by extract(year from  ps.date_Obs ),extract(month from  ps.date_Obs )", nativeQuery = true)

    public List<Object[]> getRendementEmplbetween2DateForEachMonth(@Param("start")java.sql.Date start,@Param("stop") java.sql.Date stop,@Param("matricule") String matricule);














}
