package com.example.gpro.exportFileExcel.config.view;

import com.example.gpro.ProductionHoraire.ObjectSearchProductionHoraire;
import com.example.gpro.ProductionHoraire.ProductionHoraire;
import com.example.gpro.qualité.Qualite;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ExcelViewProductionHoraire extends AbstractXlsView {
    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

        Date d = new Date();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dat = "" + dateFormat.format(d);

        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\"rapport-production-horaire-detaille.xls\"");

        @SuppressWarnings("unchecked")
        List<ProductionHoraire> productionHoraires = (List<ProductionHoraire>) model.get("productionHoraires");
        ObjectSearchProductionHoraire objectSearchProductionHoraire= (ObjectSearchProductionHoraire) model.get("objectSearch");

        // create excel xls sheet
        Sheet sheet = workbook.createSheet("ProductionHoraire Detail");
        sheet.setDefaultColumnWidth(30);


        /**************************************************/
        CellStyle style2 = workbook.createCellStyle();
        Font font1 = workbook.createFont();
        font1.setFontName("Arial");
        style2.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
        style2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font1.setBold(true);
        font1.setColor(HSSFColor.WHITE.index);
        style2.setFont(font1);

        Row header1 = sheet.createRow(0);
        header1.createCell(0).setCellValue("Date export :");
        header1.getCell(0).setCellStyle(style2);
        header1.createCell(1).setCellValue(dat);
        header1.getCell(1).setCellStyle(style2);
        header1.createCell(2).setCellValue("");
        header1.getCell(2).setCellStyle(style2);
        header1.createCell(3).setCellValue("");
        header1.getCell(3).setCellStyle(style2);
        header1.createCell(4).setCellValue("");
        header1.getCell(4).setCellStyle(style2);
        header1.createCell(5).setCellValue("");
        header1.getCell(5).setCellStyle(style2);
        header1.createCell(6).setCellValue("");
        header1.getCell(6).setCellStyle(style2);
        header1.createCell(7).setCellValue("");
        header1.getCell(7).setCellStyle(style2);


        /**************************************************/
        CellStyle style1 = workbook.createCellStyle();
        Font font2 = workbook.createFont();
        font2.setFontName("Arial");
        style1.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
        style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font2.setBold(true);
        font2.setColor(HSSFColor.WHITE.index);
        style1.setFont(font2);

        Row header3 = sheet.createRow(1);
        header3.createCell(0).setCellValue("Date Debut :");
        header3.getCell(0).setCellStyle(style1);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        String datee = date.format(objectSearchProductionHoraire.getStart());

        if(datee.equals("21-07-1983")){
            header3.createCell(1).setCellValue("Tous");
            header3.getCell(1).setCellStyle(style1);

        }else{
            header3.createCell(1).setCellValue(dateFormat.format( objectSearchProductionHoraire.getStart()));
            header3.getCell(1).setCellStyle(style1);

        }
        header3.createCell(2).setCellValue("");
        header3.getCell(2).setCellStyle(style1);
        header3.createCell(3).setCellValue("");
        header3.getCell(3).setCellStyle(style1);
        header3.createCell(4).setCellValue("");
        header3.getCell(4).setCellStyle(style1);
        header3.createCell(5).setCellValue("");
        header3.getCell(5).setCellStyle(style1);
        header3.createCell(6).setCellValue("");
        header3.getCell(6).setCellStyle(style1);
        header3.createCell(7).setCellValue("");
        header3.getCell(7).setCellStyle(style1);


        ///////////////////////////////////////////////

        Row header4 = sheet.createRow(2);
        header4.createCell(0).setCellValue("Date Fin :");
        header4.getCell(0).setCellStyle(style1);
        SimpleDateFormat datef = new SimpleDateFormat("dd-MM-yyyy");
        String date1 = datef.format(objectSearchProductionHoraire.getStop());

        if(date1.equals("21-07-1983")){
            header4.createCell(1).setCellValue("Tous");
            header4.getCell(1).setCellStyle(style1);

        }else{
            header4.createCell(1).setCellValue(dateFormat.format( objectSearchProductionHoraire.getStop()));
            header4.getCell(1).setCellStyle(style1);

        }
        header4.createCell(2).setCellValue("");
        header4.getCell(2).setCellStyle(style1);
        header4.createCell(3).setCellValue("");
        header4.getCell(3).setCellStyle(style1);
        header4.createCell(4).setCellValue("");
        header4.getCell(4).setCellStyle(style1);
        header4.createCell(5).setCellValue("");
        header4.getCell(5).setCellStyle(style1);
        header4.createCell(6).setCellValue("");
        header4.getCell(6).setCellStyle(style1);
        header4.createCell(7).setCellValue("");
        header4.getCell(7).setCellStyle(style1);


        ///////////////////////////////////////////////
        Row header5 = sheet.createRow(3);
        header5.createCell(0).setCellValue("Chaine :");
        header5.getCell(0).setCellStyle(style1);

        if(objectSearchProductionHoraire.getNomChaine().equals("")){
            header5.createCell(1).setCellValue("Tous");
            header5.getCell(1).setCellStyle(style1);

        }else{
            header5.createCell(1).setCellValue(objectSearchProductionHoraire.getNomChaine().toString());
            header5.getCell(1).setCellStyle(style1);

        }
        header5.createCell(2).setCellValue("");
        header5.getCell(2).setCellStyle(style1);
        header5.createCell(3).setCellValue("");
        header5.getCell(3).setCellStyle(style1);
        header5.createCell(4).setCellValue("");
        header5.getCell(4).setCellStyle(style1);
        header5.createCell(5).setCellValue("");
        header5.getCell(5).setCellStyle(style1);
        header5.createCell(6).setCellValue("");
        header5.getCell(6).setCellStyle(style1);
        header5.createCell(7).setCellValue("");
        header5.getCell(7).setCellStyle(style1);


        ///////////////////////////////////////////////

        Row header6 = sheet.createRow(4);
        header6.createCell(0).setCellValue("Section :");
        header6.getCell(0).setCellStyle(style1);

        if(objectSearchProductionHoraire.getNomSection().equals("")){
            header6.createCell(1).setCellValue("Tous");
            header6.getCell(1).setCellStyle(style1);

        }else{
            header6.createCell(1).setCellValue(objectSearchProductionHoraire.getNomSection().toString());
            header6.getCell(1).setCellStyle(style1);

        }
        header6.createCell(2).setCellValue("");
        header6.getCell(2).setCellStyle(style1);
        header6.createCell(3).setCellValue("");
        header6.getCell(3).setCellStyle(style1);
        header6.createCell(4).setCellValue("");
        header6.getCell(4).setCellStyle(style1);
        header6.createCell(5).setCellValue("");
        header6.getCell(5).setCellStyle(style1);
        header6.createCell(6).setCellValue("");
        header6.getCell(6).setCellStyle(style1);
        header6.createCell(7).setCellValue("");
        header6.getCell(7).setCellStyle(style1);



        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);


        // create header row
        Row header = sheet.createRow(7);

        header.createCell(0).setCellValue("Chaine");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Section");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Date");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("Heure");
        header.getCell(3).setCellStyle(style);
        header.createCell(4).setCellValue("Quantité");
        header.getCell(4).setCellStyle(style);
        header.createCell(5).setCellValue("Objectif");
        header.getCell(5).setCellStyle(style);
        header.createCell(6).setCellValue("Qte_ch2");
        header.getCell(6).setCellStyle(style);
        header.createCell(7).setCellValue("Qte_ret2");
        header.getCell(7).setCellStyle(style);


        int rowCount = 8;
        for(ProductionHoraire productionHoraire : productionHoraires){
            Row userRow =  sheet.createRow(rowCount++);
            SimpleDateFormat dateres = new SimpleDateFormat("dd-MM-yyyy");
            String dateresult = dateres.format(productionHoraire.getProductionSection().getDateObs());

            userRow.createCell(0).setCellValue(productionHoraire.getProductionSection().getSection().getChaine().getNom());
            userRow.createCell(1).setCellValue(productionHoraire.getProductionSection().getSection().getNom());
            userRow.createCell(2).setCellValue(dateresult);
            userRow.createCell(3).setCellValue(productionHoraire.getHeure());
            userRow.createCell(4).setCellValue(productionHoraire.getQuantite());
            userRow.createCell(5).setCellValue( productionHoraire.getQuantite());
            userRow.createCell(6).setCellValue(productionHoraire.getQteCh2() );
            userRow.createCell(7).setCellValue(productionHoraire.getQteRet2() );

        }






    }




}
