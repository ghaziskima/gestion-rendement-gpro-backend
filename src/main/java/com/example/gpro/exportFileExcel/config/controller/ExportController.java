package com.example.gpro.exportFileExcel.config.controller;


import com.example.gpro.ProductionHoraire.ObjectSearchProductionHoraire;
import com.example.gpro.ProductionHoraire.ProductionHoraire;
import com.example.gpro.ProductionHoraire.ProductionHoraireService;
import com.example.gpro.RendementEmploye.ObjectSearchRendement;
import com.example.gpro.RendementEmploye.RendemEntemployeService;
import com.example.gpro.RendementEmploye.RendementEmploye;
import com.example.gpro.exportFileExcel.config.view.ExcelViewProductionHoraire;
import com.example.gpro.exportFileExcel.config.view.ExcelViewQualite;
import com.example.gpro.exportFileExcel.config.view.ExcelViewRendement;
import com.example.gpro.qualité.ObjectSearchQualite;
import com.example.gpro.qualité.Qualite;
import com.example.gpro.qualité.QualiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;

import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")

@Controller
public class ExportController {

    @Autowired
    private QualiteService qualiteService;

    @Autowired
    private RendemEntemployeService rendemEntemployeService;
    @Autowired
    private ProductionHoraireService productionHoraireService;

    /**
     * Handle request to download an Excel document
     */
    @RequestMapping(value = "/downloadExcelQualite",method = RequestMethod.POST)
    public View downloadExcelQualite(Model model, @RequestBody ObjectSearchQualite objectSearchQualite) {

        List<Qualite> qualites=   qualiteService.searchQualite(objectSearchQualite);
        model.addAttribute("qualites", qualites);
        model.addAttribute("objectSearch",objectSearchQualite);

        return new ExcelViewQualite();
    }
    /*******************************************/
    /******************************************/
    @RequestMapping(value = "/downloadExcelRendement",method = RequestMethod.POST)
    public View downloadExcelRendement(Model model, @RequestBody ObjectSearchRendement objectSearchRendement) {

        List<RendementEmploye> rendements=   rendemEntemployeService.searchRendement(objectSearchRendement);
        model.addAttribute("rendements", rendements);
        model.addAttribute("objectSearch",objectSearchRendement);

        return new ExcelViewRendement();
    }
    /*******************************************/
    /******************************************/
    @RequestMapping(value = "/downloadExcelProductionHoraire",method = RequestMethod.POST)
    public View downloadExcelProductionHoraire(Model model, @RequestBody ObjectSearchProductionHoraire objectSearchProductionHoraire) {

        List<ProductionHoraire> productionHoraires=   productionHoraireService.searchPeoductionHoraire(objectSearchProductionHoraire);
        model.addAttribute("productionHoraires", productionHoraires);
        model.addAttribute("objectSearch",objectSearchProductionHoraire);

        return new ExcelViewProductionHoraire();
    }



}