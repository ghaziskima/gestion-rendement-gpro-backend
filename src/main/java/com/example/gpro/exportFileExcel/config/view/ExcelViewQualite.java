package com.example.gpro.exportFileExcel.config.view;


import com.example.gpro.qualité.ObjectSearchQualite;
import com.example.gpro.qualité.Qualite;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ExcelViewQualite extends AbstractXlsView {
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private static final String DATE_CALENDAR_FORMAT = "EEE MMM dd yyyy";

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

        Date d = new Date();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
        String dat = "" + dateFormat.format(d);


        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\"rapport-qualite-detaille.xls\"");

        @SuppressWarnings("unchecked")
        List<Qualite> qualites = (List<Qualite>) model.get("qualites");
        ObjectSearchQualite objectSearchQualite= (ObjectSearchQualite) model.get("objectSearch");


        // create excel xls sheet
        Sheet sheet = workbook.createSheet("Qualité Detail");
        sheet.setDefaultColumnWidth(30);


        /**************************************************/
        CellStyle style2 = workbook.createCellStyle();
        Font font1 = workbook.createFont();
        font1.setFontName("Arial");
        style2.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
        style2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font1.setBold(true);
        font1.setColor(HSSFColor.WHITE.index);
        style2.setFont(font1);

        Row header1 = sheet.createRow(0);
        header1.createCell(0).setCellValue("Date export :");
        header1.getCell(0).setCellStyle(style2);
        header1.createCell(1).setCellValue(dat);
        header1.getCell(1).setCellStyle(style2);
        header1.createCell(2).setCellValue("");
        header1.getCell(2).setCellStyle(style2);
        header1.createCell(3).setCellValue("");
        header1.getCell(3).setCellStyle(style2);
        header1.createCell(4).setCellValue("");
        header1.getCell(4).setCellStyle(style2);

        /**************************************************/
        CellStyle style1 = workbook.createCellStyle();
        Font font2 = workbook.createFont();
        font2.setFontName("Arial");
        style1.setFillForegroundColor(HSSFColor.CORNFLOWER_BLUE.index);
        style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font2.setBold(true);
        font2.setColor(HSSFColor.WHITE.index);
        style1.setFont(font2);

        Row header2 = sheet.createRow(1);
        header2.createCell(0).setCellValue("Matricule :");
        header2.getCell(0).setCellStyle(style1);

        if(objectSearchQualite.getMatricule().equals("")){
            header2.createCell(1).setCellValue("Tous");
            header2.getCell(1).setCellStyle(style1);

        }else{
            header2.createCell(1).setCellValue(objectSearchQualite.getMatricule());
            header2.getCell(1).setCellStyle(style1);

        }
        header2.createCell(2).setCellValue("");
        header2.getCell(2).setCellStyle(style1);
        header2.createCell(3).setCellValue("");
        header2.getCell(3).setCellStyle(style1);
        header2.createCell(4).setCellValue("");
        header2.getCell(4).setCellStyle(style1);

        ///////////////////////////////////////////////
        Row header3 = sheet.createRow(2);
        header3.createCell(0).setCellValue("Date Debut :");
        header3.getCell(0).setCellStyle(style1);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        String datee = date.format(objectSearchQualite.getStart());

        if(datee.equals("21-07-1983")){
            header3.createCell(1).setCellValue("Tous");
            header3.getCell(1).setCellStyle(style1);

        }else{
            header3.createCell(1).setCellValue(dateFormat.format( objectSearchQualite.getStart()));
            header3.getCell(1).setCellStyle(style1);

        }
        header3.createCell(2).setCellValue("");
        header3.getCell(2).setCellStyle(style1);
        header3.createCell(3).setCellValue("");
        header3.getCell(3).setCellStyle(style1);
        header3.createCell(4).setCellValue("");
        header3.getCell(4).setCellStyle(style1);

        ///////////////////////////////////////////////
        Row header4 = sheet.createRow(3);
        header4.createCell(0).setCellValue("Date Fin :");
        header4.getCell(0).setCellStyle(style1);
        SimpleDateFormat datef = new SimpleDateFormat("dd-MM-yyyy");
        String date1 = datef.format(objectSearchQualite.getStop());

        if(date1.equals("21-07-1983")){
            header4.createCell(1).setCellValue("Tous");
            header4.getCell(1).setCellStyle(style1);

        }else{
            header4.createCell(1).setCellValue(dateFormat.format( objectSearchQualite.getStop()));
            header4.getCell(1).setCellStyle(style1);

        }
        header4.createCell(2).setCellValue("");
        header4.getCell(2).setCellStyle(style1);
        header4.createCell(3).setCellValue("");
        header4.getCell(3).setCellStyle(style1);
        header4.createCell(4).setCellValue("");
        header4.getCell(4).setCellStyle(style1);

        ///////////////////////////////////////////////
        Row header5 = sheet.createRow(4);
        header5.createCell(0).setCellValue("Chaine :");
        header5.getCell(0).setCellStyle(style1);

        if(objectSearchQualite.getNomChaine().equals("")){
            header5.createCell(1).setCellValue("Tous");
            header5.getCell(1).setCellStyle(style1);

        }else{
            header5.createCell(1).setCellValue(objectSearchQualite.getNomChaine().toString());
            header5.getCell(1).setCellStyle(style1);

        }
        header5.createCell(2).setCellValue("");
        header5.getCell(2).setCellStyle(style1);
        header5.createCell(3).setCellValue("");
        header5.getCell(3).setCellStyle(style1);
        header5.createCell(4).setCellValue("");
        header5.getCell(4).setCellStyle(style1);

        ///////////////////////////////////////////////
        Row header6 = sheet.createRow(5);
        header6.createCell(0).setCellValue("Section :");
        header6.getCell(0).setCellStyle(style1);

        if(objectSearchQualite.getNomSection().equals("")){
            header6.createCell(1).setCellValue("Tous");
            header6.getCell(1).setCellStyle(style1);

        }else{
            header6.createCell(1).setCellValue(objectSearchQualite.getNomSection().toString());
            header6.getCell(1).setCellStyle(style1);

        }
        header6.createCell(2).setCellValue("");
        header6.getCell(2).setCellStyle(style1);
        header6.createCell(3).setCellValue("");
        header6.getCell(3).setCellStyle(style1);
        header6.createCell(4).setCellValue("");
        header6.getCell(4).setCellStyle(style1);



        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.LIGHT_CORNFLOWER_BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);


        // create header row
        Row header = sheet.createRow(8);
        header.createCell(0).setCellValue("Nom");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Prénom");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Matricule");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("Poste");
        header.getCell(3).setCellStyle(style);
        header.createCell(4).setCellValue("Pourcentage");
        header.getCell(4).setCellStyle(style);



        int rowCount = 9;

        for(Qualite qualite : qualites){
            Row userRow =  sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(qualite.getNom());
            userRow.createCell(1).setCellValue(qualite.getPrenom());
            userRow.createCell(2).setCellValue(qualite.getMatricule());
            userRow.createCell(3).setCellValue(qualite.getPoste());
            userRow.createCell(4).setCellValue(qualite.getPourcentage());

        }






    }



}