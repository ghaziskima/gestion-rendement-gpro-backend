package com.example.gpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class GproApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(GproApplication.class, args);
	}

}
