package com.example.gpro;

import com.example.gpro.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RolController {
    @Autowired
    private RolService rolService;
    @RequestMapping(value = "/addRole",method = RequestMethod.POST)
    public void addRole(@RequestBody Role role ){
        rolService.addRole(role);
    }



}
