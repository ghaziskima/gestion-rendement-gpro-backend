package com.example.gpro.ProductionHoraire;

import com.example.gpro.ProductionSection.ProductionSection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductionHoraireService {
    @Autowired
    private ProductionHoraireRepository productionHoraireRepository;
    @Autowired
    private SearchProductionHoraireRepository searchProductionHoraireRepository;

    public List<ProductionHoraire> getAllProductionHoraire(Long chaineId, Long sectionId, Long productionSectionId) {
        List<ProductionHoraire> productionHoraires=new ArrayList<>();
        productionHoraireRepository.findByProductionSectionIdOrderByHeure(productionSectionId).forEach(productionHoraires::add);
        return productionHoraires;
    }

    public Optional<ProductionHoraire> getProductionHoraire(Long chaineId, Long sectionId, Long productionsectionId, Long id) {
        return productionHoraireRepository.findById(id);
    }

    public void addProductionHoraire(ProductionHoraire productionHoraire) {
        productionHoraireRepository.save(productionHoraire);
    }

    public void updateProductionHoraire(Long id, ProductionHoraire productionHoraire) {
        productionHoraireRepository.save(productionHoraire);
    }

    public void deleteProductionHoraire(Long id) {
        productionHoraireRepository.deleteById(id);
    }

    public List<ProductionHoraire> getProductionHorairesOfToday(Long chaineId,Long sectionId) {
        List<ProductionHoraire> productionHoraires=new ArrayList<>();
        productionHoraireRepository.findByProductionSectionDateObsAndProductionSectionSectionChaineIdAndProductionSectionSectionIdAndProductionSectionSectionReelTrueOrderByHeure(new Date()/*java.sql.Date.valueOf("2020-07-24")*/,chaineId,sectionId).forEach(productionHoraires::add);
        // rendemEntemployeRepository.findByProductionSectionSectionChaineId(chaineId).forEach(rendementEmployes::add);
        return productionHoraires;}

    public List<Object[]> getProductionHorairesbetwenDateStartAndDateStop(java.sql.Date start, java.sql.Date stop, Long chaineId, Long sectionId) {
        List<Object[]> SumQualites=new ArrayList<>();
        productionHoraireRepository.getSumQualitesbetween2DateForEachDate(start,stop,chaineId,sectionId).forEach(SumQualites::add);
        return SumQualites;
    }
    public List<Object[]> getProductionHorairesbetwenDateStartAndDateStopOfChaineForEatchDate(java.sql.Date start, java.sql.Date stop, Long chaineId) {
        List<Object[]> SumQualites=new ArrayList<>();
        productionHoraireRepository.getSumQualitesbetween2DateOfChaineForEachDate(start,stop,chaineId).forEach(SumQualites::add);
        return SumQualites;
    }
    public List<Object[]> getProductionHorairesbetwenDateStartAndDateStopOfChaineForEatchMonth(java.sql.Date start, java.sql.Date stop, Long chaineId) {
        List<Object[]> SumQualites=new ArrayList<>();
        productionHoraireRepository.getSumQualitesbetween2DateOfChaineForEachMonth(start,stop,chaineId).forEach(SumQualites::add);
        return SumQualites;
    }

    public List<Object[]> getProductionHorairesbetwenDateStartAndDateStopForEatchMonth(java.sql.Date start, java.sql.Date stop, Long chaineId, Long sectionId) {
        List<Object[]> SumQualites=new ArrayList<>();
        productionHoraireRepository.getSumQualitesbetween2DateForEachMonth(start,stop,chaineId,sectionId).forEach(SumQualites::add);
        return SumQualites;
    }

    public List<ProductionHoraire> searchPeoductionHoraire(ObjectSearchProductionHoraire objectSearch) {
        List<ProductionHoraire> resulProductionHoraires=new ArrayList<>();

        searchProductionHoraireRepository.findProductionHoraireByObjectSearchProductionHoraire(objectSearch).forEach(resulProductionHoraires::add);
        return resulProductionHoraires;
    }

   /* public List<ProductionHoraire> getProductionHorairesbetwenDateStartAndDateStopOfChaine(java.sql.Date start, java.sql.Date stop, Long chaineId) {
        List<ProductionHoraire> productionHoraires=new ArrayList<>();
        productionHoraireRepository.findByProductionSectionDateObsBetweenAndProductionSectionSectionChaineId(start,stop,chaineId).forEach(productionHoraires::add);
        return productionHoraires;}
*/

}
