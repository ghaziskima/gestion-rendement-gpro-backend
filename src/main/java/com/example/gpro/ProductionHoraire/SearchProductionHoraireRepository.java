package com.example.gpro.ProductionHoraire;

import com.example.gpro.ProductionSection.ProductionSection;
import com.example.gpro.chaine.Chaine;
import com.example.gpro.section.Section;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository

public class SearchProductionHoraireRepository {
    EntityManager em;

    public Boolean estNonVide(Object value){
        if(value.equals("") || value==null  || value.equals("undefined")|| value.equals("null")){
            return false;
        }else{
            return true;
        }
    }
    public Boolean estNonVide(Date value){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dat = dateFormat.format(value);
        if(dat.toString().equals("") || dat==null    || dat.equals("null")|| dat.equals("21-07-1983")){
            return false;
        }else{
            return true;
        }
    }

    public SearchProductionHoraireRepository(EntityManager em) {
        this.em = em;
    }
    List<ProductionHoraire> findProductionHoraireByObjectSearchProductionHoraire(ObjectSearchProductionHoraire objectSearchProductionHoraire){
        CriteriaBuilder cb=em.getCriteriaBuilder();
        CriteriaQuery<ProductionHoraire> cq=cb.createQuery(ProductionHoraire.class);

        Root<ProductionHoraire> productionHoraireRoot=cq.from(ProductionHoraire.class);

        Join<ProductionHoraire,ProductionSection> join1=productionHoraireRoot.join("productionSection", JoinType.INNER);
        Join<ProductionSection,Section> join2=join1.join("section",JoinType.INNER);
        Join<Section,Chaine> join3=join2.join("chaine",JoinType.INNER);

        Predicate  stratPredicate=cb.greaterThanOrEqualTo(join1.get("dateObs"),objectSearchProductionHoraire.getStart());
        Predicate  stopPredicate=cb.lessThanOrEqualTo(join1.get("dateObs"),objectSearchProductionHoraire.getStop());
        Predicate  nomsectionPredicate=cb.equal(join2.get("nom"),objectSearchProductionHoraire.getNomSection());
        Predicate nomchainePredicate=cb.equal(join3.get("nom"),objectSearchProductionHoraire.getNomChaine());

        List<Predicate> whereClause = new ArrayList<>();

        if (estNonVide(objectSearchProductionHoraire.getNomChaine())){
            whereClause.add(nomchainePredicate);
        }
        if (estNonVide(objectSearchProductionHoraire.getNomSection())){
            whereClause.add(nomsectionPredicate);
        }

        if (estNonVide(objectSearchProductionHoraire.getStart())){
            whereClause.add(stratPredicate);
        }


        if (estNonVide(objectSearchProductionHoraire.getStop())){
            whereClause.add(stopPredicate);
        }

        cq.where(whereClause.toArray(new Predicate[]{}));
        cq.orderBy(cb.asc(join1.get("dateObs")),cb.asc(join3.get("nom")) ,cb.asc(join2.get("nom"))  , cb.asc(productionHoraireRoot.get("heure")) );


        TypedQuery<ProductionHoraire> query=em.createQuery(cq);
        return query.getResultList();




    }




    }
