package com.example.gpro.ProductionHoraire;

import com.example.gpro.chaine.Chaine;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ProductionHoraireRepository extends CrudRepository<ProductionHoraire,Long>{
    public List<ProductionHoraire> findByProductionSectionIdOrderByHeure(Long productionSectionId);

    public List<ProductionHoraire> findByProductionSectionDateObsAndProductionSectionSectionChaineIdAndProductionSectionSectionIdAndProductionSectionSectionReelTrueOrderByHeure(Date date, Long ChaineId, Long sectionId);
    public List<ProductionHoraire> findByProductionSectionDateObsAndProductionSectionSectionChaineIdAndProductionSectionSectionReelTrueOrderByHeure(Date date, Long ChaineId);

   // public List<ProductionHoraire> findByProductionSectionDateObsBetweenAndProductionSectionSectionChaineIdAndProductionSectionSectionId(java.sql.Date start, java.sql.Date stop, Long ChaineId, Long sectionId);
    //public List<ProductionHoraire> findByProductionSectionDateObsBetweenAndProductionSectionSectionChaineId(java.sql.Date start, java.sql.Date stop, Long ChaineId);

    @Query(value = "select  ps.date_Obs as dateObs, sum(quantite)as sumQuantite,ch.nom as nom  from Production_Horaire ph " +
            "inner join Production_Section ps " +
            "on ps.id = ph.production_section_id " +
            "inner join Section s " +
            "on s.id = ps.section_id " +
            "inner join Chaine ch " +
            "on ch.id = s.chaine_id " +
            "WHERE ch.id = :chaineId AND s.id = :sectionId "+
            "AND :stop >= ps.date_Obs AND ps.date_Obs >= :start  "+
            "group by  ch.nom,ps.date_Obs order by ps.date_Obs", nativeQuery = true)
    public List<Object[]> getSumQualitesbetween2DateForEachDate(@Param("start")java.sql.Date start,@Param("stop") java.sql.Date stop,@Param("chaineId") Long chaineId,@Param("sectionId") Long sectionId);

    @Query(value = "select  ps.date_Obs as dateObs, sum(quantite)as sumQuantite,ch.nom as nom from Production_Horaire ph " +
            "inner join Production_Section ps " +
            "on ps.id = ph.production_section_id " +
            "inner join Section s " +
            "on s.id = ps.section_id " +
            "inner join Chaine ch " +
            "on ch.id = s.chaine_id " +
            "WHERE ch.id = :chaineId  "+
            "AND :stop >= ps.date_Obs AND ps.date_Obs >= :start  "+
            "group by ch.nom, ps.date_Obs order by ps.date_Obs", nativeQuery = true)
    public List<Object[]> getSumQualitesbetween2DateOfChaineForEachDate(@Param("start")java.sql.Date start,@Param("stop") java.sql.Date stop,@Param("chaineId") Long chaineId);



    @Query(value = "select  extract(year from  ps.date_Obs ) as yearDateObs,extract(month from  ps.date_Obs )as monthDateObs, sum(quantite)as sumQuantite,ch.nom as nom from Production_Horaire ph " +
            "inner join Production_Section ps " +
            "on ps.id = ph.production_section_id " +
            "inner join Section s " +
            "on s.id = ps.section_id " +
            "inner join Chaine ch " +
            "on ch.id = s.chaine_id " +
            "WHERE ch.id = :chaineId  "+
            "AND :stop >= ps.date_Obs AND ps.date_Obs >= :start  "+
            "group by ch.nom,extract(year from  ps.date_Obs ),extract(month from  ps.date_Obs ) order by extract(year from  ps.date_Obs ),extract(month from  ps.date_Obs )", nativeQuery = true)
    public List<Object[]> getSumQualitesbetween2DateOfChaineForEachMonth(@Param("start")java.sql.Date start,@Param("stop") java.sql.Date stop,@Param("chaineId") Long chaineId);

    @Query(value = "select extract(year from  ps.date_Obs ) as yearDateObs,extract(month from  ps.date_Obs )as monthDateObs, sum(quantite)as sumQuantite,ch.nom as nom from Production_Horaire ph " +
            "inner join Production_Section ps " +
            "on ps.id = ph.production_section_id " +
            "inner join Section s " +
            "on s.id = ps.section_id " +
            "inner join Chaine ch " +
            "on ch.id = s.chaine_id " +
            "WHERE ch.id = :chaineId  AND s.id = :sectionId "+
            "AND :stop >= ps.date_Obs AND ps.date_Obs >= :start  "+
            "group by ch.nom,extract(year from  ps.date_Obs ),extract(month from  ps.date_Obs ) order by extract(year from  ps.date_Obs ),extract(month from  ps.date_Obs )", nativeQuery = true)
    public List<Object[]> getSumQualitesbetween2DateForEachMonth(@Param("start")java.sql.Date start,@Param("stop") java.sql.Date stop,@Param("chaineId") Long chaineId,@Param("sectionId") Long sectionId);


}
