package com.example.gpro.ProductionHoraire;

import com.example.gpro.ProductionSection.ProductionSection;
import com.example.gpro.chaine.Chaine;
import com.example.gpro.section.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;
@CrossOrigin(origins = "*", allowedHeaders = "*")

@RestController
public class ProductionHoraireController {

    @Autowired
    private ProductionHoraireService productionHoraireService;
    @RequestMapping("ProductionHorairesbetween2DateOfMonth/{chaineId}/{start}/{stop}")
    public List<Object[]> getProductionHorairesbetwenDateStartAndDateStopOfChaineForEatchMonth(@PathVariable Long chaineId,  @PathVariable java.sql.Date start, @PathVariable java.sql.Date stop){
        return productionHoraireService.getProductionHorairesbetwenDateStartAndDateStopOfChaineForEatchMonth(start,stop,chaineId);
    }
    @RequestMapping("ProductionHorairesbetween2DateOfMonth/{chaineId}/{sectionId}/{start}/{stop}")
    public List<Object[]> getProductionHorairesbetwenDateStartAndDateStopForEatchMonth(@PathVariable Long chaineId,@PathVariable Long sectionId,  @PathVariable java.sql.Date start, @PathVariable java.sql.Date stop){
        return productionHoraireService.getProductionHorairesbetwenDateStartAndDateStopForEatchMonth(start,stop,chaineId,sectionId);
    }
    @RequestMapping("ProductionHorairesbetween2Date/{chaineId}/{start}/{stop}")
    public List<Object[]> getProductionHorairesbetwenDateStartAndDateStopOfChaineForEatchDate(@PathVariable Long chaineId,  @PathVariable java.sql.Date start, @PathVariable java.sql.Date stop){
        return productionHoraireService.getProductionHorairesbetwenDateStartAndDateStopOfChaineForEatchDate(start,stop,chaineId);
    }
    @RequestMapping("ProductionHorairesbetween2Date/{chaineId}/{sectionId}/{start}/{stop}")
    public List<Object[]> getProductionHorairesbetwenDateStartAndDateStop(@PathVariable Long chaineId,@PathVariable Long sectionId,  @PathVariable java.sql.Date start, @PathVariable java.sql.Date stop){
        return productionHoraireService.getProductionHorairesbetwenDateStartAndDateStop(start,stop,chaineId,sectionId);
    }
   /* @RequestMapping("ProductionHorairesbetween2DateOfChaine/{chaineId}/{start}/{stop}")
    public List<ProductionHoraire> getProductionHorairesbetwenDateStartAndDateStopOfChaine(@PathVariable Long chaineId,  @PathVariable java.sql.Date start, @PathVariable java.sql.Date stop){
        return productionHoraireService.getProductionHorairesbetwenDateStartAndDateStopOfChaine(start,stop,chaineId);
    }*/
    @RequestMapping("ProductionHorairesOfToday/{chaineId}/{sectionId}")
    public List<ProductionHoraire> getProductionHorairesOfToday(@PathVariable Long chaineId,@PathVariable Long sectionId){
        return productionHoraireService.getProductionHorairesOfToday(chaineId,sectionId);
    }

    @RequestMapping("/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionSectionId}/productionhoraires")
    public List<ProductionHoraire> getAllProductionHoraire(@PathVariable Long chaineId, @PathVariable Long sectionId,@PathVariable Long productionSectionId){
        return productionHoraireService.getAllProductionHoraire(chaineId,sectionId,productionSectionId);
    }
    @RequestMapping("/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/productionhoraires/{id}")
    public Optional<ProductionHoraire> getProductionHoraire(@PathVariable Long chaineId, @PathVariable Long sectionId,@PathVariable Long productionsectionId, @PathVariable Long id){
        return productionHoraireService.getProductionHoraire(chaineId,sectionId,productionsectionId,id);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/productionhoraires", method = RequestMethod.POST)
    public void addProductionHoraire(@RequestBody ProductionHoraire productionHoraire, @PathVariable Long chaineId, @PathVariable Long sectionId,@PathVariable Long productionsectionId){
        productionHoraire.setProductionSection(new ProductionSection(productionsectionId,new Date(),new Section(sectionId,"",false,new Chaine(chaineId,"",null),null),null,null,null));
        productionHoraireService.addProductionHoraire(productionHoraire);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/productionhoraires/{id}", method = RequestMethod.PUT)
    public void updateProductionHoraire(@RequestBody ProductionHoraire productionHoraire, @PathVariable Long chaineId, @PathVariable Long sectionId,@PathVariable Long productionsectionId ,@PathVariable Long id){
        productionHoraire.setProductionSection(new ProductionSection(productionsectionId,new Date(),new Section(sectionId,"",false,new Chaine(chaineId,"",null),null),null,null,null));
        productionHoraireService.updateProductionHoraire(id,productionHoraire);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/productionhoraires/{id}", method = RequestMethod.DELETE)
    public void deleteProductionHoraire(@PathVariable Long id){
        productionHoraireService.deleteProductionHoraire(id);
    }

    @RequestMapping(value = "resultProductionHoraire",method = RequestMethod.POST)
    public List<ProductionHoraire> searchProductionHoraire(@RequestBody ObjectSearchProductionHoraire objectSearch){

        return productionHoraireService.searchPeoductionHoraire(objectSearch);
    }

}
