package com.example.gpro.ProductionHoraire;

import java.util.Date;

public class ObjectSearchProductionHoraire {
    private Date start;
    private Date stop;
    private String nomChaine;
    private String nomSection;


    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

    public String getNomChaine() {
        return nomChaine;
    }

    public void setNomChaine(String nomChaine) {
        this.nomChaine = nomChaine;
    }

    public String getNomSection() {
        return nomSection;
    }

    public void setNomSection(String nomSection) {
        this.nomSection = nomSection;
    }
}
