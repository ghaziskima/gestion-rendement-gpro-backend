package com.example.gpro.ProductionHoraire;

import com.example.gpro.ProductionSection.ProductionSection;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
public class ProductionHoraire {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",updatable = false,nullable = false)

    Long id;

    int heure;

    int quantite;

    int objectif;

    int qteRet2;


    int qteCh2;

    @ManyToOne
    private ProductionSection productionSection;


    public ProductionHoraire(Long id,int heure, int quantite, int objectif, int qteRet2, int qteCh2, ProductionSection productionSection) {
        this.id=id;
        this.heure=heure;
        this.quantite = quantite;
        this.objectif = objectif;
        this.qteRet2 = qteRet2;
        this.qteCh2 = qteCh2;
        this.productionSection = productionSection;
    }


    public ProductionHoraire() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getHeure() {
        return heure;
    }

    public void setHeure(int heure) {
        this.heure = heure;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public int getObjectif() {
        return objectif;
    }

    public void setObjectif(int objectif) {
        this.objectif = objectif;
    }

    public int getQteRet2() {
        return qteRet2;
    }

    public void setQteRet2(int qteRet2) {
        this.qteRet2 = qteRet2;
    }

    public int getQteCh2() {
        return qteCh2;
    }

    public void setQteCh2(int qteCh2) {
        this.qteCh2 = qteCh2;
    }

    public ProductionSection getProductionSection() {
        return productionSection;
    }

    public void setProductionSection(ProductionSection productionSection) {
        this.productionSection = productionSection;
    }
}
