package com.example.gpro.itemAffichage;

import com.example.gpro.affichage.Affichage;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
public class ItemAffichage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",updatable = false,nullable = false)



    Long id;
    @NotNull
    @Valid
    String nom;
    @NotNull
    @Valid
    int ord;

    Long idChaine;

    @ManyToOne
    private Affichage affichage;

    public ItemAffichage(Long id, String nom, int ord, Affichage affichage,Long idChaine) {
        this.id=id;
        this.nom = nom;
        this.ord = ord;
        this.affichage = affichage;
        this.idChaine=idChaine;
    }

    public ItemAffichage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getOrd() {
        return ord;
    }

    public void setOrd(int ord) {
        this.ord = ord;
    }

    public Affichage getAffichage() {
        return affichage;
    }

    public void setAffichage(Affichage affichage) {
        this.affichage = affichage;
    }

    public Long getIdChaine() {
        return idChaine;
    }

    public void setIdChaine(Long idChaine) {
        this.idChaine = idChaine;
    }
}
