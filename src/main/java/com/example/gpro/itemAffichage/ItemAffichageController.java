package com.example.gpro.itemAffichage;

import com.example.gpro.affichage.Affichage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")

@RestController
public class ItemAffichageController {
    @Autowired
    private ItemAffichageService itemAffichageService;

    @RequestMapping("/affichages/{id}/items")
    public List<ItemAffichage> GetAllItemAffichages(@PathVariable Long id){
        return itemAffichageService.getAllItemAffichage(id);
    }

    @RequestMapping(value = "/affichages/{afficheId}/items", method = RequestMethod.POST)

    public void addItemAffiche(@RequestBody ItemAffichage itemAffichage, @PathVariable Long afficheId){
        itemAffichage.setAffichage(new Affichage(afficheId,"",null));
        itemAffichageService.addItemAffiche(itemAffichage);
    }

    @RequestMapping(value = "/affichages/{afficheId}/items/{id}", method = RequestMethod.PUT)
    public void updateItemAffiche(@RequestBody ItemAffichage itemAffichage, @PathVariable Long id, @PathVariable Long afficheId){
        itemAffichage.setAffichage(new Affichage(afficheId,"",null));
        itemAffichageService.updateItemAffiche(id, itemAffichage);


    }
    @RequestMapping(value = "/affichages/{afficheId}/items/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id){

        itemAffichageService.deleteItemAffiche(id);
    }

}
