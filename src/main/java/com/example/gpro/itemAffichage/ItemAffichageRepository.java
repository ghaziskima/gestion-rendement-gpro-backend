package com.example.gpro.itemAffichage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemAffichageRepository extends CrudRepository<ItemAffichage,Long>{
    public List<ItemAffichage> findAllByAffichageIdOrderByOrdAsc(Long affichageId);

}
