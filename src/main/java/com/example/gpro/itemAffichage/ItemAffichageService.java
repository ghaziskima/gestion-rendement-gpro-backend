package com.example.gpro.itemAffichage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemAffichageService {
    @Autowired
    private ItemAffichageRepository itemAffichageRepository;

    public List<ItemAffichage> getAllItemAffichage(Long id) {
        List<ItemAffichage> itemAffichages=new ArrayList<>();
        itemAffichageRepository.findAllByAffichageIdOrderByOrdAsc(id).forEach(itemAffichages::add);
        return itemAffichages;


    }

    public void addItemAffiche(ItemAffichage itemAffichage) {
        itemAffichageRepository.save(itemAffichage);
    }

    public void updateItemAffiche(Long id, ItemAffichage itemAffichage) {
        itemAffichageRepository.save(itemAffichage);
    }


    public void deleteItemAffiche(Long id) {
        itemAffichageRepository.deleteById(id);
    }
}
