package com.example.gpro.chaine;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface chaineRepository extends CrudRepository<Chaine,Long> {


}
