package com.example.gpro.chaine;

import com.example.gpro.section.Section;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Chaine {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",updatable = false,nullable = false)



    Long id;
    @NotNull
    @Valid
    String nom;

    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "chaine")
    @JsonIgnore

    private List<Section> sections=new ArrayList<>();



    public Chaine(Long id,String nom, List<Section> sections) {
        this.id=id;
        this.nom = nom;
        this.sections = sections;
    }

    public Chaine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }
}
