package com.example.gpro.chaine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@CrossOrigin(origins = "*", allowedHeaders = "*")

@RestController
public class chaineController {
    @Autowired
    private chaineService ChaineService;
    @Autowired
    private chaineRepository ChaineRepository;


    @RequestMapping("/chaines")
    public List<Chaine> GetAllChaine(){
        return ChaineService.getAllChaine();
    }
    @RequestMapping("/chaines/{id}")
    public Optional<Chaine> GetChaine(@PathVariable Long id){
        return ChaineService.getChaine(id);
    }
    @RequestMapping(value = "/chaines",method = RequestMethod.POST)

    public void addChaine(@RequestBody Chaine chaine){
        ChaineService.addChaine(chaine);
    }
    @RequestMapping(value = "/chaines/{id}",method = RequestMethod.PUT)
    public void updateChaine(@RequestBody Chaine chaine, @PathVariable Long id){
        ChaineService.updateChainr(id,chaine);
    }
    @RequestMapping(value = "/chaines/{id}",method = RequestMethod.DELETE)
    public void deleteChaine( @PathVariable Long id){
        ChaineService.deleteChaine(id);
    }

}
