package com.example.gpro.chaine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class chaineService {
    @Autowired
    private chaineRepository ChaineRepository;


    public List<Chaine> getAllChaine() {
        List<Chaine> chaines=new ArrayList<>();
        ChaineRepository.findAll().forEach(chaines::add);
        return chaines;


    }

    public Optional<Chaine> getChaine(Long id) {
        return ChaineRepository.findById(id);
    }

    public void addChaine(Chaine chaine) {
        ChaineRepository.save(chaine);
    }

    public void updateChainr(Long id, Chaine chaine) {
        ChaineRepository.save(chaine);
    }

    public void deleteChaine(Long id) {
        ChaineRepository.deleteById(id);
    }


}
