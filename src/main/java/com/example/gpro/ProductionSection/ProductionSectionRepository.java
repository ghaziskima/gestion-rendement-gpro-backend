package com.example.gpro.ProductionSection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ProductionSectionRepository extends CrudRepository<ProductionSection,Long> {
    public List<ProductionSection> findBySectionId(Long sectionId);
    /*@Query("select p from ProductionSection p where p.dateObs like :x")
    public List<ProductionSection> productionSectionPardatObs(@Param("x") Date dateObs);*/
    public List<ProductionSection> findByDateObs(java.sql.Date date);

    public List<ProductionSection> findAllByOrderByDateObsDescSectionChaineNomAsc();









}
