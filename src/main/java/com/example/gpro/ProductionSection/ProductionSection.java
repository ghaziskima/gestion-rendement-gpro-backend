package com.example.gpro.ProductionSection;

import com.example.gpro.ProductionHoraire.ProductionHoraire;
import com.example.gpro.RendementEmploye.RendementEmploye;
import com.example.gpro.qualité.Qualite;
import com.example.gpro.section.Section;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class ProductionSection {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",updatable = false,nullable = false)

    Long id;

    @Temporal(TemporalType.DATE)
    @NotNull
    @Valid
    Date dateObs;
    @ManyToOne
    private Section section;

    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "productionSection")
    @JsonIgnore
    private List<Qualite> qualites=new ArrayList<>();
    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "productionSection")
    @JsonIgnore
    private List<RendementEmploye> rendementEmployes=new ArrayList<>();
    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "productionSection")
    @JsonIgnore
    private List<ProductionHoraire> productionHoraires=new ArrayList<>();



    public ProductionSection(Long id,Date dateObs, Section section, List<Qualite> qualites, List<RendementEmploye> rendementEmployes, List<ProductionHoraire> productionHoraires) {
        this.id=id;
        this.dateObs = dateObs;
        this.section = section;
        this.qualites = qualites;
        this.rendementEmployes = rendementEmployes;
        this.productionHoraires = productionHoraires;
    }

    public ProductionSection() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateObs() {
        return dateObs;
    }

    public void setDateObs(Date dateObs) {
        this.dateObs = dateObs;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public List<Qualite> getQualites() {
        return qualites;
    }

    public void setQualites(List<Qualite> qualites) {
        this.qualites = qualites;
    }

    public List<RendementEmploye> getRendementEmployes() {
        return rendementEmployes;
    }

    public void setRendementEmployes(List<RendementEmploye> rendementEmployes) {
        this.rendementEmployes = rendementEmployes;
    }

    public List<ProductionHoraire> getProductionHoraires() {
        return productionHoraires;
    }

    public void setProductionHoraires(List<ProductionHoraire> productionHoraires) {
        this.productionHoraires = productionHoraires;
    }


}
