package com.example.gpro.ProductionSection;

import com.example.gpro.ProductionHoraire.ProductionHoraire;
import com.example.gpro.chaine.Chaine;
import com.example.gpro.section.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
@CrossOrigin(origins = "*", allowedHeaders = "*")

@RestController
public class ProductionSectionController {

    @Autowired
    private ProductionSectionService productionSectionService;

    @RequestMapping("/productionsectionsdate")
    public List<ProductionSection> getProd(){
        return productionSectionService.getRend();
    }
    @RequestMapping("/productionsections")
    public List<ProductionSection> getAllProductionSection(){
        return productionSectionService.getAllProductionSections();
    }
    @RequestMapping("/productionsections/{id}")
    public Optional<ProductionSection> ProductionSection( @PathVariable Long id){
        return productionSectionService.getUnProductionSection(id);
    }
    @RequestMapping("/chaines/{chaineId}/sections/{sectionId}/productionsections")
    public List<ProductionSection> getAllProductionSectionsDeSection(@PathVariable Long chaineId,@PathVariable Long sectionId){
        return productionSectionService.getAllProductionSectionsDeSection(chaineId,sectionId);
    }
    @RequestMapping("/chaines/{chaineId}/sections/{sectionId}/productionsections/{id}")
    public Optional<ProductionSection> getProductionSection(@PathVariable Long chaineId, @PathVariable Long sectionId, @PathVariable Long id){
        return productionSectionService.getProductionSection(chaineId,sectionId,id);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections", method = RequestMethod.POST)
    public void addProductionSection(@RequestBody ProductionSection productionSection, @PathVariable Long chaineId, @PathVariable Long sectionId){

        productionSection.setSection(new Section(sectionId,"",false,new Chaine(chaineId,"",null),null));

        productionSectionService.addProductionSection(productionSection);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{id}", method = RequestMethod.PUT)
    public void updateProductionSection(@RequestBody ProductionSection productionSection, @PathVariable Long chaineId,@PathVariable Long id, @PathVariable Long sectionId){
        productionSection.setSection(new Section(sectionId,"",false,new Chaine(chaineId,"",null),null));
        productionSectionService.updateProductionSection(id,productionSection);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{id}", method = RequestMethod.DELETE)
    public void deleteProductionSection(@PathVariable Long id){
        productionSectionService.deleteProductionSection(id);
    }
}


