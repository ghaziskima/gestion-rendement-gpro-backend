package com.example.gpro.ProductionSection;

import com.example.gpro.ProductionHoraire.ProductionHoraire;
import com.example.gpro.ProductionHoraire.ProductionHoraireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductionSectionService {
    @Autowired
    private ProductionSectionRepository productionSectionRepository;


    public List<ProductionSection> getAllProductionSectionsDeSection(Long chaineId, Long sectionId) {
        List<ProductionSection> productionSections=new ArrayList<>();
        productionSectionRepository.findBySectionId(sectionId).forEach(productionSections::add);
        return productionSections;
    }

    public Optional<ProductionSection> getProductionSection(Long chaineId, Long sectionId, Long id) {
        return productionSectionRepository.findById(id);
    }

    public void addProductionSection(ProductionSection productionSection) {
       /* List<ProductionHoraire> productionHoraires=new ArrayList<>();
        for (int i=1;i<=9;i++) {

            productionHoraires.add(new ProductionHoraire(null, i, 0, 0, 0, 0, null));
            productionHoraireService.addProductionHoraire(new ProductionHoraire(null, i, 0, 0, 0, 0, productionSection));
        }
        productionSection.setProductionHoraires(productionHoraires);
        */

        productionSectionRepository.save(productionSection);
    }

    public void updateProductionSection(Long id, ProductionSection productionSection) {
        productionSectionRepository.save(productionSection);
    }

    public void deleteProductionSection(Long id) {
        productionSectionRepository.deleteById(id);
    }

    public List<ProductionSection> getAllProductionSections() {
        List<ProductionSection> productionSections=new ArrayList<>();
        productionSectionRepository.findAllByOrderByDateObsDescSectionChaineNomAsc().forEach(productionSections::add);
        return productionSections;
    }

    public Optional<ProductionSection> getUnProductionSection(Long id) {
        return productionSectionRepository.findById(id);
    }

    public List<ProductionSection> getRend() {
        List<ProductionSection> productionSections=new ArrayList<>();
        productionSectionRepository.findByDateObs(java.sql.Date.valueOf("2020-07-24")).forEach(productionSections::add);
        System.out.print(new Date());
        return productionSections;
    }


}
