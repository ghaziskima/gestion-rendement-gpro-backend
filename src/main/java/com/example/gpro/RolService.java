package com.example.gpro;

import com.example.gpro.model.Role;
import com.example.gpro.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolService {
    @Autowired
    private RoleRepository roleRepository;
    public void addRole(Role role) {
        roleRepository.save(role);

    }
}
