package com.example.gpro.section;

import com.example.gpro.ProductionSection.ProductionSection;
import com.example.gpro.chaine.Chaine;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Section {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",updatable = false,nullable = false)

    Long id;
    @NotNull
    @Valid
    String nom;
    @NotNull
    @Valid
    Boolean reel;

    @ManyToOne
    private Chaine chaine;

    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "section")
    @JsonIgnore
    private List<ProductionSection> productionSection=new ArrayList<>();



    public Section(Long id,String nom, Boolean reel, Chaine chaine, List<ProductionSection> productionSection) {
        this.id=id;
        this.nom = nom;
        this.reel = reel;
        this.chaine = chaine;
        this.productionSection = productionSection;
    }

    public Section() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Boolean getReel() {
        return reel;
    }

    public void setReel(Boolean reel) {
        this.reel = reel;
    }

    public Chaine getChaine() {
        return chaine;
    }

    public void setChaine(Chaine chaine) {
        this.chaine = chaine;
    }

    public List<ProductionSection> getProductionSection() {
        return productionSection;
    }

    public void setProductionSection(List<ProductionSection> productionSection) {
        this.productionSection = productionSection;
    }
}
