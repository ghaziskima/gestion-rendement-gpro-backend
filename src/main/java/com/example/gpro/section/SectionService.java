package com.example.gpro.section;

import com.example.gpro.chaine.Chaine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SectionService {
    @Autowired
    private SectionRepository sectionRepository;

    public List<Section> getSectionOfReelTrue(Long chaineId) {
        List<Section> sections=new ArrayList<>();
        sectionRepository.findByChaineIdAndReelTrue(chaineId).forEach(sections::add);
        return sections;
    }

    public List<Section> getAllSection(Long id) {
        List<Section> sections=new ArrayList<>();
        sectionRepository.findByChaineId(id).forEach(sections::add);
        return sections;
    }

    public Optional<Section> getSection(Long id) {
        return sectionRepository.findById(id);
    }

    public void addSection( Section section) {
        sectionRepository.save(section);
    }

    public void updateSection(Long chaineId, Section section) {
        sectionRepository.save(section);
    }

    public void deleteSection(Long id) {
        sectionRepository.deleteById(id);
    }
}
