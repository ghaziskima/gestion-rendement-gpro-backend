package com.example.gpro.section;

import com.example.gpro.chaine.Chaine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@CrossOrigin(origins = "*", allowedHeaders = "*")

@RestController
public class SectionController {

    @Autowired
    private SectionService sectionService;

    @RequestMapping("{chaineId}/getSectionOfReelTrue")
    public List<Section> getSectionOfReelTrue(@PathVariable Long chaineId){
        return sectionService.getSectionOfReelTrue(chaineId);
    }

    @RequestMapping("/chaines/{id}/sections")
    public List<Section> getAllSection(@PathVariable Long id){
        return sectionService.getAllSection(id);
    }
    @RequestMapping("/chaines/{chaineId}/sections/{id}")
    public Optional<Section> getSection(@PathVariable Long id){
        return sectionService.getSection(id);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections", method = RequestMethod.POST)
    public void addSection(@RequestBody Section section, @PathVariable Long chaineId){
        section.setChaine(new Chaine(chaineId,"",null));
         sectionService.addSection(section);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{id}", method = RequestMethod.PUT)
    public void updateSection(@RequestBody Section section, @PathVariable Long chaineId,@PathVariable Long id){
        section.setChaine(new Chaine(chaineId,"",null));
        sectionService.updateSection(chaineId,section);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{id}", method = RequestMethod.DELETE)
    public void deleteSection(@PathVariable Long id){
        sectionService.deleteSection(id);
    }

}
