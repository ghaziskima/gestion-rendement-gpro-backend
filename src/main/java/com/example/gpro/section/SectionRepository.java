package com.example.gpro.section;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SectionRepository extends CrudRepository<Section,Long>{
    public List<Section> findByChaineId(Long chaineId);

    public List<Section> findByChaineIdAndReelTrue(Long chaineId);

}
