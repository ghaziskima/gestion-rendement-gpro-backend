package com.example.gpro.qualité;

import com.example.gpro.ProductionSection.ProductionSection;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
public class Qualite {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",updatable = false,nullable = false)

    Long id;

    String nom;

    String prenom;
    String matricule;
    String poste;
    @NotNull
    @Valid
    Float pourcentage;

    @ManyToOne
    private ProductionSection productionSection;



    public Qualite(Long id,String nom, String prenom, String matricule,String poste, Float pourcentage, ProductionSection productionSection) {
        this.id=id;
        this.nom = nom;
        this.prenom = prenom;
        this.matricule=matricule;
        this.poste = poste;
        this.pourcentage = pourcentage;
        this.productionSection = productionSection;
    }

    public Qualite() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public Float getPourcentage() {
        return pourcentage;
    }


    public void setPourcentage(Float pourcentage) {
        this.pourcentage = pourcentage;
    }

    public ProductionSection getProductionSection() {
        return productionSection;
    }

    public void setProductionSection(ProductionSection productionSection) {
        this.productionSection = productionSection;
    }
}
