package com.example.gpro.qualité;

import com.example.gpro.ProductionSection.ProductionSection;
import com.example.gpro.chaine.Chaine;
import com.example.gpro.section.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")

@RestController
public class QualiteController {


    @Autowired
    private QualiteService qualiteService;
    @RequestMapping("qualiteOfSectionFalse")
    public List<Qualite> getQualiteOfSectionFalse(){
        return qualiteService.getQualiteOfSectionFalse();
    }
    @RequestMapping("qualiteOfSectionTrue")
    public List<Qualite> getQualiteOfSectionTrue(){
        return qualiteService.getQualiteOfSectionTrue();
    }
    @RequestMapping("/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionSectionId}/qualites")
    public List<Qualite> getAllQualites(@PathVariable Long chaineId, @PathVariable Long sectionId, @PathVariable Long productionSectionId){
        return qualiteService.getAllQualites(chaineId,sectionId,productionSectionId);
    }
    @RequestMapping("/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/qualites/{id}")
    public Optional<Qualite> getQualite(@PathVariable Long chaineId, @PathVariable Long sectionId, @PathVariable Long productionsectionId, @PathVariable Long id){
        return qualiteService.getQualite(chaineId,sectionId,productionsectionId,id);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/qualites", method = RequestMethod.POST)
    public void addQualite(@RequestBody Qualite qualite, @PathVariable Long chaineId, @PathVariable Long sectionId, @PathVariable Long productionsectionId){
        qualite.setProductionSection(new ProductionSection(productionsectionId,new Date(),new Section(sectionId,"",false,new Chaine(chaineId,"",null),null),null,null,null));
        qualiteService.addQualite(qualite);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/qualites/{id}", method = RequestMethod.PUT)
    public void updateQualite(@RequestBody Qualite qualite, @PathVariable Long chaineId, @PathVariable Long sectionId,@PathVariable Long productionsectionId ,@PathVariable Long id){
        qualite.setProductionSection(new ProductionSection(productionsectionId,new Date(),new Section(sectionId,"",false,new Chaine(chaineId,"",null),null),null,null,null));
        qualiteService.updateQualite(id,qualite);
    }
    @RequestMapping(value = "/chaines/{chaineId}/sections/{sectionId}/productionsections/{productionsectionId}/qualites/{id}", method = RequestMethod.DELETE)
    public void deleteQualite(@PathVariable Long id){
        qualiteService.deleteQualite(id);
    }


    @RequestMapping(value = "resultQualite",method = RequestMethod.POST)
    public List<Qualite> searchQualite(@RequestBody ObjectSearchQualite objectSearch){

        return qualiteService.searchQualite(objectSearch);
    }




    /*
    ************************
    * **********************
     */


}
