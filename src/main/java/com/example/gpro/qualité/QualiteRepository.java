package com.example.gpro.qualité;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface QualiteRepository extends CrudRepository<Qualite,Long> {
    public List<Qualite> findByProductionSectionId(Long productionSectionId);

    public List<Qualite> findByProductionSectionDateObsAndProductionSectionSectionReelTrue(Date date);

    public List<Qualite> findByProductionSectionDateObsAndProductionSectionSectionReelFalse(Date date);

}
