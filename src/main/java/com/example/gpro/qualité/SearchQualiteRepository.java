package com.example.gpro.qualité;

import com.example.gpro.ProductionSection.ProductionSection;
import com.example.gpro.chaine.Chaine;
import com.example.gpro.section.Section;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class SearchQualiteRepository {

    EntityManager em;


    public Boolean estNonVide(Object value){
        if(value.equals("") || value==null  || value.equals("undefined")|| value.equals("null")){
            return false;
        }else{
            return true;
        }
    }
    public Boolean estNonVide(Date value){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dat = dateFormat.format(value);
        if(dat.toString().equals("") || dat==null    || dat.equals("null")|| dat.equals("21-07-1983")){
            return false;
        }else{
            return true;
        }
    }




    public SearchQualiteRepository(EntityManager em) {
        this.em = em;
    }

    List<Qualite> findQualiteByObjectSearchQualite(ObjectSearchQualite objectSearchQualite){
        CriteriaBuilder cb=em.getCriteriaBuilder();
        CriteriaQuery<Qualite> cq=cb.createQuery(Qualite.class);

        Root<Qualite> qualiteRoot=cq.from(Qualite.class);

        Join<Qualite,ProductionSection> join1=qualiteRoot.join("productionSection",JoinType.INNER);
        Join<ProductionSection,Section> join2=join1.join("section",JoinType.INNER);
        Join<Section,Chaine> join3=join2.join("chaine",JoinType.INNER);

        Predicate matriculePredicate=cb.equal(qualiteRoot.get("matricule"),objectSearchQualite.getMatricule());
        Predicate  stratPredicate=cb.greaterThanOrEqualTo(join1.get("dateObs"),objectSearchQualite.getStart());
        Predicate  stopPredicate=cb.lessThanOrEqualTo(join1.get("dateObs"),objectSearchQualite.getStop());
        Predicate  nomsectionPredicate=cb.equal(join2.get("nom"),objectSearchQualite.getNomSection());
        Predicate nomchainePredicate=cb.equal(join3.get("nom"),objectSearchQualite.getNomChaine());





        List<Predicate> whereClause = new ArrayList<>();

        if(estNonVide(objectSearchQualite.getMatricule())){
            whereClause.add(matriculePredicate);
        }
        if (estNonVide(objectSearchQualite.getNomChaine())){
            whereClause.add(nomchainePredicate);
        }
        if (estNonVide(objectSearchQualite.getNomSection())){
            whereClause.add(nomsectionPredicate);
        }

        if (estNonVide(objectSearchQualite.getStart())){
                whereClause.add(stratPredicate);
        }


        if (estNonVide(objectSearchQualite.getStop())){
                whereClause.add(stopPredicate);
        }



        cq.where(whereClause.toArray(new Predicate[]{}));








        TypedQuery<Qualite> query=em.createQuery(cq);
        return query.getResultList();




    }
}
