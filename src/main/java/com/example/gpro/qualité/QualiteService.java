package com.example.gpro.qualité;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class QualiteService {
    @Autowired
    private QualiteRepository qualiteRepository;
    @Autowired
    private SearchQualiteRepository searchQualiteRepository;

    public List<Qualite> getQualiteOfSectionTrue() {
        List<Qualite> qualites=new ArrayList<>();
        qualiteRepository.findByProductionSectionDateObsAndProductionSectionSectionReelTrue(new Date()).forEach(qualites::add);
        return qualites;
    }
    public List<Qualite> getQualiteOfSectionFalse() {
        List<Qualite> qualites=new ArrayList<>();
        qualiteRepository.findByProductionSectionDateObsAndProductionSectionSectionReelFalse(new Date()).forEach(qualites::add);
        return qualites;
    }

    public List<Qualite> getAllQualites(Long chaineId, Long sectionId, Long productionSectionId) {
        List<Qualite> qualites=new ArrayList<>();
        qualiteRepository.findByProductionSectionId(productionSectionId).forEach(qualites::add);
        return qualites;
    }

    public Optional<Qualite> getQualite(Long chaineId, Long sectionId, Long productionsectionId, Long id) {
        return qualiteRepository.findById(id);
    }

    public void addQualite(Qualite qualite) {
        qualiteRepository.save(qualite);
    }

    public void updateQualite(Long id, Qualite qualite) {
        qualiteRepository.save(qualite);
    }

    public void deleteQualite(Long id) {
        qualiteRepository.deleteById(id);
    }


    public List<Qualite> searchQualite(ObjectSearchQualite objectSearchQualite) {
        List<Qualite> resulQqualites=new ArrayList<>();

        searchQualiteRepository.findQualiteByObjectSearchQualite(objectSearchQualite).forEach(resulQqualites::add);
        return resulQqualites;
    }
}
