package com.example.gpro.affichage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AffichageRepository extends CrudRepository<Affichage,Long>{
    Optional<Affichage> findByNom(String nom);


}
