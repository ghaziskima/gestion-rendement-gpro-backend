package com.example.gpro.affichage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AffichageService {

    @Autowired
    private AffichageRepository affichageRepository;

    public List<Affichage> getAllAffichage() {
        List<Affichage> affichages=new ArrayList<>();
         affichageRepository.findAll().forEach(affichages::add);
         return affichages;
    }

    public Optional<Affichage> getAffichage(Long id) {
        return affichageRepository.findById(id);
    }


    public void addAffichage(Affichage affichage) {
        affichageRepository.save(affichage);
    }

    public void updateAffichage(Long id, Affichage affichage) {
        affichageRepository.save(affichage);
    }

    public void deleteAffichage(Long id) {
        affichageRepository.deleteById(id);
    }

    public Optional<Affichage> GetAffichageByName(String nom) {
        return affichageRepository.findByNom(nom);
    }
}
