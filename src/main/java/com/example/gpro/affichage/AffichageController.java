package com.example.gpro.affichage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@CrossOrigin(origins = "*", allowedHeaders = "*")

@RestController
public class AffichageController {
    @Autowired
    private AffichageService affichageService;


    @RequestMapping("/affichages")
    public List<Affichage> GetAllAffichages(){
        return affichageService.getAllAffichage();
    }

    @RequestMapping("/affichage/{nom}")
    public Optional<Affichage> GetAffichageByName(@PathVariable String nom ){
        return affichageService.GetAffichageByName(nom);
    }

    @RequestMapping("/affichages/{id}")
    public Optional<Affichage> GetAffichage(@PathVariable Long id){
        return affichageService.getAffichage(id);
    }

    @RequestMapping(value = "/affichages",method = RequestMethod.POST)

    public void addAffichage(@RequestBody Affichage affichage ){
        affichageService.addAffichage(affichage);
    }
    @RequestMapping(value = "/affichages/{id}",method = RequestMethod.PUT)
    public void updateAffichage(@RequestBody Affichage affichage , @PathVariable Long id){
        affichageService.updateAffichage(id,affichage);
    }
    @RequestMapping(value = "/affichages/{id}",method = RequestMethod.DELETE)
    public void deleteAffichage( @PathVariable Long id){
        affichageService.deleteAffichage(id);
    }


}
