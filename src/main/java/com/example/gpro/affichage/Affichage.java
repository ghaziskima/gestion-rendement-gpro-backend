package com.example.gpro.affichage;
import com.example.gpro.itemAffichage.ItemAffichage;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Affichage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",updatable = false,nullable = false)



    Long id;
    @NotNull
    @Valid
    String nom;
    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "affichage")
    @JsonIgnore
    private List<ItemAffichage> itemAffichages=new ArrayList<>();


   /* @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "productionSection")
    @JsonIgnore
    private List<Qualite> qualites=new ArrayList<>();
    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "productionSection")
    @JsonIgnore
    private List<RendementEmploye> rendementEmployes=new ArrayList<>();
    @OneToMany(cascade = CascadeType.REMOVE,mappedBy = "productionSection")
    @JsonIgnore
    private List<ProductionHoraire> productionHoraires=new ArrayList<>();*/



    public Affichage(Long id,String nom,List<ItemAffichage> itemAffichages/*,  List<Qualite> qualites, List<RendementEmploye> rendementEmployes, List<ProductionHoraire> productionHoraires*/) {
        this.id=id;
        this.nom = nom;
        this.itemAffichages=itemAffichages;
      /*  this.qualites = qualites;
        this.rendementEmployes = rendementEmployes;
        this.productionHoraires = productionHoraires;*/
    }



    public Affichage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<ItemAffichage> getItemAffichages() {
        return itemAffichages;
    }

    public void setItemAffichages(List<ItemAffichage> itemAffichages) {
        this.itemAffichages = itemAffichages;
    }
/*  public List<Qualite> getQualites() {
        return qualites;
    }

    public void setQualites(List<Qualite> qualites) {
        this.qualites = qualites;
    }

    public List<RendementEmploye> getRendementEmployes() {
        return rendementEmployes;
    }

    public void setRendementEmployes(List<RendementEmploye> rendementEmployes) {
        this.rendementEmployes = rendementEmployes;
    }

    public List<ProductionHoraire> getProductionHoraires() {
        return productionHoraires;
    }

    public void setProductionHoraires(List<ProductionHoraire> productionHoraires) {
        this.productionHoraires = productionHoraires;
    }*/
}
